#include "verbose.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

int main(int argc, char* argv[])
{

    if (argc != 2) {
        std::cerr << "Usage: interpreter [FILENAME]\n";
        
        return 1;
    }
    
    std::string file_name = argv[1];
    std::ifstream program_file(file_name);
    std::stringstream b;

    if (!program_file.good()) {
        std::cerr << "Error: file \"" << file_name << "\" cannot be opened\n";

        return 2;
    }

    b << program_file.rdbuf();
    Interpreter verbose(b.str());

    return verbose.run();
}
