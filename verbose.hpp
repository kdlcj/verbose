#ifndef VERBOSE
#define VERBOSE

#include <memory>
#include <string>
#include <vector>
#include <iostream>

class Interpreter {
private:
    enum Instruction {
        _load_constant = 0,
        _load_address,
        _store_address,

        _constant_symbol,
        _dereference_symbol,
        _terminal_symbol,

        _output,
        _output_char,
        _input,
        _input_char,

        _inc,
        _dec,

        _exit,

        _jump,
        _jump_if_zero,
        _jump_if_a_eq_b
    };

    static const int _instruction_count = _jump_if_a_eq_b + 1;
    unsigned char _registers[3];
    static const int _continue = -1;
    size_t _address = 0;
    std::vector<unsigned char> _memory;
public: //DEBUG
    Instruction as_instruction(char, char) const;

    size_t as_constant(char) const;

    void remove_small(std::string&, size_t = 4);

    size_t next_word(size_t, const std::string&) const;

    bool terminal(char) const;

    bool letter(char) const;

    void load_constant();

    void load_address();

    void store_address();

    void input(bool);

    void output(bool);

    void jump();

    void jump_if_zero();

    void jump_if_a_eq_b();

    void inc();

    void dec();

    int exit();

    int arity(Instruction i) const;

    std::string name(Instruction i) const;

public:

    Interpreter() = default;

    Interpreter(std::string);

    Interpreter(const Interpreter&);

    Interpreter(const std::vector<unsigned char>&);

    Interpreter& operator=(const Interpreter&);

    void load(std::string);
    
    void print_instruction_set() const;

    void print_instruction_set_md() const;

    void print_constants() const;

    void print_as_program() const;

    friend std::ostream& operator<<(std::ostream&, const Interpreter&);

    int step(bool = false);

    int run();
};

#endif //VERBOSE

