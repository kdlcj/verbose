verbose
======

### verbose is a simple experimental/joke/esoteric programming language and its interpreter.


##### The goal of verbose is to design a language of a which a source code looks just like any other natural language text.

verbose source code consists of series of words - word being a series of letters, separarated by non-letter characters.
Letter is any letter of latin alphabet, i.e. 'a'-'z' and the upper case forms. Not all words are functional, only words of
lengts four characters and more are counted as such, other words are simply ignored by interpreter.

Verbose has sixteen instructions of various arity. Each instruction is determined by the first letters of two consecutive words.
Each operand of the instruction is calculated from the words followed by the instruction words as series of encoded decimal numerals terminated
by special terminal word, the terminal word is also determined by its first letter, which is marked in the table of constants as terminal.

This methods enables using large number of options to encode a single instruction, which is desired,
as the source code is supposed to look and to feel like a text of natural language.

### Example 'Hello World' program
The following is a traditional Hello World program. As the name suggests, it is very verbose, even
for such a small and simple program.
```
Kings of international and atrocious veracity astonished hourly vortexious zealotry.
Onions are left on the jaden asterisks, but heavy kilograms of butter, the traditional
attitude of serious venery. Rings of martyrdom are lofty and jingling on quarantines usurped 
by an angry kick notoriously left autocracy kickup, but Eleanor kindly united those, who cite
Jeremy.

Hastingly, Theresa wrongly proposed ghostly bringing. "Zerings are wrong," Peter after long
minutes krasped. Frankly, Eleanor married long hours Jackson, bringing yvelious macaroni
levitating trapedly. Knowingly citadels tamed analogous mavericks, aposteriour wrinkles
and wroughts. Pinging ceramics never zeniths wings of prosperous things, as sophomore 
kings.

Furthermore, Eleanor announced marriage is moderately jossy. Her brother yourstly meeked,  
and adamant diamonds kloops. Bringing a yellow mercy, loots traveled kilometres clinging,
thereafter, absolutely long litographical warnings are bewarning the young matadors and
lest kroops coopered, the tigers loops are wrecked.
```

### Compilation
Code is simply compiled by ``cppcompiler -std=c++11 main.cpp verbose.cpp``

Program is run with the program text file as its first argument: ``./interpreter hello_world``

#### Instruction set table
|#  |name                  | arity | description|
|---|----------------------|-------|------------|
|0  |load constant         | 2     |firts parameter determines to which register is the constant going to be loaded, second is the constant itself|
|1  |load address          | 2     |parameter usage same as above|
|2  |store address         | 2     |parameter usage same as above|
|3  |constant symbol       | 1     ||
|4  |dereference symbol    | 0     |dereference symbol is deprecated|
|5  |terminal symbol       | 0     ||
|6  |output                | 1     |parameter determines which register is going to be printed out (as numerical value)|
|7  |output as char        | 1     |the same, but as character|
|8  |input                 | 1     ||
|9  |input as char         | 1     ||
|10 |increment             | 1     |parameter determines register|
|11 |decrement             | 1     ||
|12 |exit                  | 0     |returns value currently stored in register A|
|13 |jump                  | 1     |parameters determines new address|
|14 |jump if zero          | 1     ||
|15 |jump if a eq b        | 1     |compares registers for equality and in positive case jumps to parametrised address|



#### Words To Instruction conversion table
This table shows encoding of two words into one instruction. Each number represents instruction
as stated in Instruction set table. Row is first, column is second; for example instruction "Alan Turing" is number 9.
This information can be provided by ``print_instruction_set()`` function.

|   | a| b | c | d | e | f | g | h | i | j | k | l | m | n | o | p | q | r | s | t | u | v | w | x | y | z |
|---|--|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
| a |6 |7  | 8 | 9 |10 |11 |12 |13 |14 |15 | 0 |1  | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |10 |11 |12 |13 |14 |15 |
| b |11|12 |13 |14 |15 |0  | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10|11 |12 |13 |14 |15 | 0 | 1 | 2 | 3 | 4 |
| c |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |
| d |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |
| e |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |
| f |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |
| g |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |
| h |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |
| i |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |
| j |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |
| k |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |
| l |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |
| m |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |
| n |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |
| o |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |
| p |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |
| q |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |
| r |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |
| s |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |
| t |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |
| u |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |
| v |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |
| w |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |
| x|9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |
| y |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |
| z |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |13 |14 |15 |0 |1 |2 |3 |4 |5 |6 |7 |8 |9 |10 |11 |12 |

#### Character To Constant conversion table
When writing constants, this table is needed. Note that the values greater or equal to 10 are considered
to be terminal symbols.
This information is provided by ``print_constants()`` function.

|character|number| 
|---|--|
|a|1|
|b|2|
|c|3|
|d|4|
|e|5|
|f|6|
|g|7|
|h|8|
|i|9|
|j|10|
|k|11|
|l|0|
|m|1|
|n|2|
|o|3|
|p|4|
|q|5|
|r|6|
|s|7|
|t|8|
|u|9|
|v|10|
|w|11|
|x|0|
|y|11|
|z|12|
