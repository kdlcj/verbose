#include "verbose.hpp"

#include <memory>
#include <iostream>


Interpreter::Interpreter(std::string s) { load(s); }

Interpreter::Interpreter(const Interpreter& i) : _memory(i._memory) {}

Interpreter::Interpreter(const std::vector<unsigned char>& m) : _memory(m) {}

Interpreter& Interpreter::operator=(const Interpreter& rhs)
{
    _memory = rhs._memory; 
    return *this;
}

std::ostream& operator<<(std::ostream& os, const Interpreter& in)
{
    auto i = 0;
    for (auto c : in._memory) { os << +c << ":" << i++ << " "; }

    os << "\n"; 

    for (int i = 0; i < 3; ++i) {
        os << "reg" << i << " " << +in._registers[i] << "  "; 
    }

    return os;
}

void Interpreter::print_as_program() const
{
    size_t position = 0;
    Instruction ins;
    while (position < _memory.size()) {
        ins = (Instruction) _memory[position];
        std::cout << position << "\t" << name(ins) << std::endl;

        for (auto i = 0; i < arity((Instruction) _memory[position]); ++i) {
            auto index = position + i + 1;
            std::cout << index << "\t" << +_memory[index] << std::endl;
        }

        position += arity(ins) + 1;
    }
}


/*  executes one(1) instruction from _memory, which is
 *  pointed at by the _address variable, and then modifies
 *  _address variable accordingly
 */
int Interpreter::step(bool info)
{
    int return_value = _continue;

    if (_address >= _memory.size()) { return return_value - 1; }

    if (info) {
        auto instruction = (Instruction) _memory[_address];
        auto a = arity(instruction);
        auto n = name(instruction);

        std::cout << "at " << _address << " is " << n << " ";

        for (auto i = 0; i < a; i++) {
            std::cout << +_memory[_address + i + 1] << " ";
        }
        std::cout << std::endl;

        for (auto i = 0; i < 3; i++) {
            std::cout << "reg" << i << " " << +_registers[i] << " ";
        }
        std::cout << "\n" << *this << "\n\n";
    }

    switch (_memory[_address]) {
    case _constant_symbol:
        _address += 2; 
        break;
    case _load_address:
        load_address();
        break;
    case _load_constant: 
        load_constant();
        break;
    case _store_address:
        store_address();
        break;
    case _output:
        output(false);
        break;
    case _output_char:
        output(true);
        break;
    case _input:
        input(false);
        break;
    case _input_char:
        input(true);
        break;
    case _jump:
        jump();
        break;
    case _jump_if_zero:
        jump_if_zero();
        break;
    case _jump_if_a_eq_b:
        jump_if_a_eq_b();
        break;
    case _inc:
        inc();
        break;
    case _dec:
        dec();
        break;
    case _exit:
        return_value = exit();
        break;
    default:
        std::cerr << "unknown instruction " << +_memory[_address] << "\n";
        return_value = 10;
    }

    return return_value;
}

int Interpreter::run()
{
    auto return_value = _continue;
    while (return_value == _continue) {
        return_value = step();
    }

    return return_value < 0 ? -return_value : return_value;
}

/*  loads the program in text format into memory;
 *  i.e. parses the text into a internal representation
 */
void Interpreter::load(std::string input_text)
{
    Instruction instruction;
    auto size = input_text.size();
    size_t position = 0;
    char letters[2];
    remove_small(input_text);

    while (!letter(input_text[position])) { ++position; }

    while (position < size) {
        for (auto& l : letters) { //get instruction
            l = input_text[position];
            position = next_word(position, input_text);
        }

        instruction = as_instruction(letters[0], letters[1]);
        if (instruction != _constant_symbol) {
            _memory.push_back(instruction);
        }

        for (auto i = 0; i < arity(instruction); ++i) { //get operands
            auto operand = 0;

            while (position < size && !terminal(input_text[position])) {
                operand *= 10;
                operand += as_constant(input_text[position]);
                position = next_word(position, input_text);
            }
            position = next_word(position, input_text);
            _memory.push_back(operand);
            next_word(position, input_text);
        }
    }
}

size_t Interpreter::as_constant(char c) const
{
    size_t r = 0;

    if (c >= 'a' && c <= 'z') {
        r = (c - 'a' + 1) % 12;
    } else if (c >= 'A' && c <= 'Z') {
        r = (c - 'A' + 1) % 12;
    }

    switch (c) {
    case 'z':
    case 'Z':
        r = 12;
        break;
    case 'y':
    case 'Y':
        r = 11;
        break;
    }

    return r;
}

Interpreter::Instruction Interpreter::as_instruction(char a, char b) const
{
    int i = (a * (5 +_instruction_count) + b) % _instruction_count;
    return (Instruction) i;
}

bool Interpreter::letter(char c) const
{
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

bool Interpreter::terminal(char c) const
{
    return as_constant(c) >= 10; 
}

/*  returns index of the next word
 */
size_t Interpreter::next_word(size_t position, const std::string& string) const
{
    size_t new_position = position;
    auto size = string.size();

    if (letter(string[new_position])) {
        while (new_position < size && letter(string[new_position])) { ++new_position; }
    } 

    while (new_position < size && !letter(string[new_position])) { ++new_position; }

    return new_position;
}

/*  replaces all words of smaller size than "min" parameter
 *  with white space; used to ignore short words in a program
 */
void Interpreter::remove_small(std::string& string, size_t min)
{
    size_t counter = 0, position = 0, look;
    
    if (!letter(string[position])) {
        next_word(position, string); 
    }
   
    look = position;
    
    while (position < string.size()) {
        if (letter(string[look++])) {
            ++counter;
        } else {
            if (counter < min) {
                while (position != look) {
                    string[position++] = ' ';
                }
            } else {
                position = look;
            }
            counter = 0;
        }
    }
}

void Interpreter::load_constant()
{
    _registers[_memory[_address + 1]] = _memory[_address + 2];
    _address += 3;
}

void Interpreter::load_address()
{
    _registers[_memory[_address + 1]] = _memory[_memory[_address + 2]];
    _address += 3;
}

void Interpreter::store_address()
{
    _memory[_memory[_address + 2]] = _registers[_memory[_address + 1]];
    _address += 3;
}

void Interpreter::input(bool as_character)
{
    auto register_number = _memory[++_address % 3];
    std::cin >> _registers[register_number];
    if (!as_character) {
        _registers[register_number] -= '0';
    }
    ++_address;
}

void Interpreter::output(bool as_character)
{
    if (as_character) {
        std::cout << _registers[_memory[_address + 1]];
    } else {
        std::cout << +_registers[_memory[_address + 1]];
    }
    _address += 2;
}

void Interpreter::jump()
{
    _address = _memory[_address + 1];
}

void Interpreter::jump_if_zero()
{
    if (_registers[0] == 0) {
        _address = _memory[_address + 1];
    } else {
        _address += 2;
    }
}

void Interpreter::jump_if_a_eq_b()
{
    if (_registers[0] == _registers[1]) {
        _address = _memory[_address + 1];
    } else {
        _address += 2;
    }
}

void Interpreter::inc()
{
    ++_registers[_memory[++_address]];
    ++_address;
}

void Interpreter::dec()
{
    --_registers[_memory[++_address]];
    ++_address;
}

int Interpreter::exit()
{
    return _registers[0];
}

void Interpreter::print_constants() const
{
    for (auto a = 'a'; a <= 'z'; a++) {
        std::cout << a << " -> " << as_constant(a) << std::endl;
    }
}

void Interpreter::print_instruction_set() const
{
    for (auto a = 'a'; a <= 'z'; a++) {
        for (auto b = 'a'; b <= 'z'; b++) {
            auto instruction = as_instruction(a, b);
            std::cout << a << " " << b << " -> "
                      << instruction << "\t"
                      << name(instruction)
                      << "(" << arity(instruction) << ")" << "\n";
        }
    }
}

void Interpreter::print_instruction_set_md() const
{
    for (auto a = 'a'; a <= 'z'; a++) {
        std::cout << a << "|";
        for (auto b = 'a'; b <= 'z'; b++) {
            auto instruction = as_instruction(a, b);
            std::cout << instruction << " |";
        }
        std::cout << "\n";
    }
}


int Interpreter::arity(Instruction i) const
{
    switch(i) {
    case _dereference_symbol:
    case _terminal_symbol:
    case _exit:
        return 0;
        break;
    case _inc:
    case _dec:
    case _output_char:
    case _output:
    case _input:
    case _input_char:
    case _constant_symbol:
    case _jump:
    case _jump_if_zero:
    case _jump_if_a_eq_b:
        return 1;
        break;
    case _load_constant:
    case _load_address:
    case _store_address:
        return 2;
        break;
    default:
        return 0;
    }
}

std::string Interpreter::name(Instruction i) const
{
    switch(i) {
    case _dereference_symbol:
        return "_dereference_symbol";
    case _terminal_symbol:
        return "_terminal_symbol";
    case _exit:
        return "_exit";
    case _inc:
        return "_inc";
    case _dec:
        return "_dec";
    case _output_char:
        return "_output_char";
    case _output:
        return "_output";
    case _input:
        return "_input";
    case _input_char:
        return "_input_char";
    case _constant_symbol:
        return "_constant_symbol";
    case _jump:
        return "_jump";
    case _jump_if_zero:
        return "_jump_if_zero";
    case _jump_if_a_eq_b:
        return "_jump_if_a_eq_b";
    case _load_constant:
        return "_load_constant";
    case _load_address:
        return "_load_address";
    case _store_address:
        return "_store_address";
    default:
        return "unknown instruction";
    }
}

